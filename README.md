# Sanbercode-Perpustakaan Backend Microservices with Python

Ini adalah tugas akhir di Pelatihan SANBERCODE kelas Backend Microservices with Python. Tugas akhir ini menerapkan konsep Backend Microservices di sebuah aplikasi, aplikasi yang digunakan adalah aplikasi perpustakaan.

Aplikasi Perpustakaan ini terdiri dari 3 file utama, yaitu:
app.py
books.py
bestsellers-with-categories.csv

Biodata pembuat:
Ginanjar Setyo Nugroho
Email:
ginanjarsetyonugroho@gmail.co
